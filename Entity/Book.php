<?php

class Book
{
    public string $full_name;
    public string $email;
    public string $phone;
    public string $address;
    public string $bookName;

    /**
     * @param string $full_name
     * @param string $email
     * @param string $phone
     * @param string $address
     * @param string $bookName
     */
    public function __construct(string $full_name, string $email, string $phone, string $address, string $bookName)
    {
        $this->full_name = $full_name;
        $this->email = $email;
        $this->phone = $phone;
        $this->address = $address;
        $this->bookName = $bookName;
    }

}